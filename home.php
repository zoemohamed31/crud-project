<!DOCTYPE html>
<html lang="en">
	<head>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	</head>
	<body>
		<?php
		session_start();
		include "koneksi.php";
		
		if(!isset($_SESSION["login"])){
		header("Location:index.php");
		exit();
		}
		if(isset($_GET["pesan"])){
			if($_GET["pesan"] == "berhasil_hapus"){
				$pesan = "Berhasil menghapus data";
				$warna = "success";
			}
			if($_GET["pesan"] ==  "gagal_hapus"){
				$pesan = "Gagal menghapus data";
				$warna = "danger";
			}
		}
		$username=$_SESSION["username"];
		$sql = "SELECT * FROM users";
		$result = mysqli_query($conn,$sql);
		$hasil = mysqli_fetch_all($result,MYSQLI_ASSOC);
		
		?>
		<style>
		.table th {
		text-align: center;
		}
		.table {
		border-radius: 5px;
		width:90%;
		margin: 90px auto;
		float: none;
		}
		body{
			background-image:url('grey.jpg');
			overflow:hidden;
			
		}
		
		
		
		</style>
		
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNavDropdown">
				<ul class="navbar-nav">
					<li class="nav-item active">
						<a class="nav-link" href="home.php" style="color:blue;" style="font-weight:bold;">Home</a>
					</li>
					<li class="nav-item active">
						<a   href="daftar.php"  class="nav-link" style="color:blue;">Tambah Data</a>
					</li>
					<li class="nav-item active">
						<a href="keluar.php"   class="nav-link" style="text-decoration:none;" style="color:blue;"  >Logout</a>
					</li>
					
					
				</ul>
			</div>
		</nav>
		
		<?php
		if(isset($pesan)){
		?>
		<div class = "alert alert-<?= $warna; ?>" role="alert">
			<h5 style="text-align:center;"><?= $pesan; ?></h5>
		</div>
		<?php
		}
		?>
		<div class="row justify-content-center">
			
			<table class="table table-bordered table-dark">
				<thead>
					<tr>
						<th>No</th>
						<th>Fullname</th>
						<th>Username</th>
						<th>Email</th>
						<th>Action</th>
					</tr>
					
				</thead>
				<tbody>
					<?php
					foreach($hasil as $key => $user_data){
					
					?>
					<tr>
						<td><?= $key + 1 ?></td>
						<td><?= $user_data["fullname"]?></td>
						<td><?= $user_data["username"] ?></td>
						<td><?= $user_data["email"] ?></td>
						<td>
							<a href="form_ubah.php?id=<?=$user_data['id']?>" class="btn btn-primary">Ubah</a>
							<a href="proses_hapus.php?id=<?=$user_data['id']?>" class="btn btn-danger">Hapus</a>
						</td>
					</tr>
					<?php
					}
					?>
				</tbody>
			</table>
		</div>
		<!-- JS -->
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	</body>
</html>